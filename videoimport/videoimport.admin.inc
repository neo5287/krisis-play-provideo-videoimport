<?php

/**
 * @file
 * Administration pages
 *
 */


function videoimport_settings() {
  $directory = variable_get('videoimport_path', 'files');
  if (!is_dir($directory)) {
    if (@drupal_mkdir($directory, NULL, TRUE)) {
      drupal_chmod($directory);
      drupal_set_message(t('Directory of "!directory" is created',array('!directory' => $directory)));
    }
  }
  $fields = field_info_fields();
  //drupal_set_message('<pre>'. check_plain(print_r(field_info_fields(), 1)) .'</pre>');
  $a_field = $a_type = array();
  foreach ($fields as $key => $data) {
    if ($data['module'] == 'video') {
      $a_field[$key] = $key;
      if (isset($data['bundles']['node']) and is_array($data['bundles']['node']) and count($data['bundles']['node'])) {
        foreach ($data['bundles']['node'] as $tdata) {
          $a_type[$tdata] = $tdata;
        }
      }
    }
  }
  $form['videoimport_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Specify the path to the directory on a server from which files will be imported'),
    '#default_value' => variable_get('videoimport_path', 'files'),
    '#description' => t('Specify a path to a directory on a server from which files will be imported.'),
  );
  $form['videoimport_extensions'] = array(
    '#type' => 'textfield',
    '#title' => t('Extensions to import'),
    '#default_value' => variable_get('videoimport_extensions', 'avi mpg mpeg mov flv mp4'),
    '#description' => t('Specify file extensions that are allowed to import and use space as a separator.'),
  );
  $form['videoimport_type'] = array(
    '#type' => 'select',
    '#title' => 'Content Type',
    '#default_value' => variable_get('videoimport_type', 'video'),
    '#options' => $a_type,
    '#description' => t('Select a content type that will be created when importing.'),
  );
  $form['videoimport_field'] = array(
    '#type' => 'select',
    '#title' => 'Video Field',
    '#default_value' => variable_get('videoimport_field', 'field_video'),
    '#options' => $a_field,
    '#description' => t('Select a field that will be used when importing video files.'),
  );
  $form['videoimport_activate_nodes'] = array(
    '#type' => 'checkbox',
    '#title' => t('Automatically activate newly created nodes.'),
    '#default_value' => variable_get('videoimport_activate_nodes', FALSE),
  );
  $form['videoimport_delete_files'] = array(
    '#type' => 'checkbox',
    '#title' => t('Remove files after import.'),
    '#default_value' => variable_get('videoimport_delete_files', FALSE),
  );

  return system_settings_form($form);
}
